using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Enemy S.O.")]
public class EnemySO : ScriptableObject
{
    public string enemyName;
    public string enemyDescription;
    public int damage;
    public int hp;
    public float velocity;
    public float cooldown;
    public Sprite sprite;
    public float size;
    public int probablyItem; // probabilidades de soltar un Item      1/1  ....  1/3   .... 1/5
    public int experience;
    
}
