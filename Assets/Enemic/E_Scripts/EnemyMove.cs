using UnityEngine;

public class EnemyMove : MonoBehaviour
{

    Vector3 direction;
    private Rigidbody2D rb;
    private float scale;
    private float velocity;

    public void Start()
    {
        velocity = GetComponent<EnemyInfo>().velocity;
        rb = GetComponent<Rigidbody2D>();
        scale = transform.localScale.x;
    }
    public void Follow(Vector3 pointToMove)
    {
        direction = (pointToMove - transform.position);
        /* girar mu�eco */
        if (direction.x < 0) transform.localEulerAngles = Vector3.zero;
        else if (direction.x > 0) transform.localEulerAngles = Vector3.up*180;
        /* ----- ------ */
        rb.velocity = direction.normalized * velocity;
    }
    
}
