using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : ScriptableObject
{
    [SerializeField] private Sprite sprite;
    [SerializeField] private float hp;
    [SerializeField] private float velocity;

}
