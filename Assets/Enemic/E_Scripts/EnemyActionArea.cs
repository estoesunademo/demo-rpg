using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActionArea : MonoBehaviour
{
    //observado
    public Action<Transform> OnEnter;
    public Action<Transform> OnExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            OnEnter?.Invoke(collision.gameObject.transform);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            OnExit?.Invoke(collision.gameObject.transform);
        }
    }
}
