using Jugador;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(EnemyMove))] /* componente movimiento */
[RequireComponent(typeof(EnemyFight))] /* componente lucha */
[RequireComponent(typeof(EnemyInfo))] /* componente info */
public class EnemyMachineStates : MonoBehaviour
{
    [SerializeField] private EnemyDetectionArea detectionArea;
    [SerializeField] private EnemyActionArea actionArea;
    [SerializeField] private EnemyHitBox enemyHitbox;

    private Rigidbody2D m_Rigidbody;
    private List<Transform> playersTransform = new();
    private List<Transform> playersFighting = new();


    /* ---------- AWAKE, START, UPDATE, ONDESTROY ---------- */
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        /* suscripciones */
        detectionArea.OnEnter += OnEnterDetection;
        detectionArea.OnExit += OnExitDetection;
        actionArea.OnEnter += OnEnterAction;
        actionArea.OnExit += OnExitAction;
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnDestroy()
    {
        /* desuscripciones */
        detectionArea.OnEnter -= OnEnterDetection;
        detectionArea.OnExit -= OnExitDetection;
        actionArea.OnEnter -= OnEnterAction;
        actionArea.OnExit -= OnExitAction;
    }

    /*  -------------   M�QUINA DE ESTADOS  ----------------*/
    public enum SwitchMachineStates
    {
        NONE,
        IDLE,
        FOLLOW,
        FIGHT,
        HURT,
        PATROL
    };

    public SwitchMachineStates m_CurrentState;

    /* * ------------------------------------------------ * */
    public void ChangeState(SwitchMachineStates newState)
    {
        ExitState();
        InitState(newState);
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;

            case SwitchMachineStates.FOLLOW:
                break;

            case SwitchMachineStates.FIGHT:

                GetComponent<EnemyFight>().StopFight();
                break;

            default:
                break;
        }
    }

    public void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = Vector2.zero;
                break;

            case SwitchMachineStates.FOLLOW:
                break;

            case SwitchMachineStates.FIGHT:
                m_Rigidbody.velocity = Vector2.zero;
                enemyHitbox.damage = GetComponent<EnemyInfo>().damage;
                GetComponent<EnemyFight>().StartFight();
                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;

            case SwitchMachineStates.FOLLOW:
                /*else*/
                GetComponent<EnemyMove>().Follow(FollowTheCloser().position);
                break;

            case SwitchMachineStates.FIGHT:
                break;

            default:
                break;
        }
    }

    /*  -------------------  EVENTOS ----------------------*/
    private void OnEnterDetection(Transform player)
    {
        playersTransform.Add(player);
        if (m_CurrentState != SwitchMachineStates.FIGHT)
            ChangeState(SwitchMachineStates.FOLLOW);
    }

    private void OnExitDetection(Transform player)
    {
        playersTransform.Remove(player);
        if (playersTransform.Count == 0) ChangeState(SwitchMachineStates.IDLE);
    }

    private void OnEnterAction(Transform player)
    {
        if (m_CurrentState == SwitchMachineStates.FOLLOW ||
            m_CurrentState == SwitchMachineStates.FIGHT)
        {
            playersFighting.Add(player);
            ChangeState(SwitchMachineStates.FIGHT);
        }
    }

    private void OnExitAction(Transform player)
    {
        playersFighting.Remove(player);
        if (playersFighting.Count == 0)
        {
            ChangeState(SwitchMachineStates.FOLLOW);
        }
    }

    //mira la distancia mas corta para perseguir
    private Transform FollowTheCloser()
    {
        if (playersTransform.Count == 1)
        {
            return playersTransform[0];
        }

        var position = transform.position;
        return (playersTransform[0].position - position).magnitude
               <= (playersTransform[1].position - position).magnitude
            ? playersTransform[0]
            : playersTransform[1];
    }
}