using System;
using UnityEngine;

public class EnemyInfo : MonoBehaviour
{

    [SerializeField] private EnemySO enemyData;

    [Header("FICHA T�CNICA")]
    private string enemyName;
    private string enemyDescription;
    public int hp;
    public int damage;
    public float velocity;
    public float cooldown;
    private Sprite sprite;
    private float size;
    public int probability;
    public int experience;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        InitEnemy();
        spriteRenderer = GetComponent<SpriteRenderer>();
        
        cooldown = 2f;

        transform.localScale = new Vector3(size, size, 0);
        spriteRenderer.sprite = sprite;
    }

    private void InitEnemy()
    {
        enemyName = enemyData.enemyName;
        enemyDescription = enemyData.enemyDescription;
        damage = enemyData.damage;
        hp = enemyData.hp;
        velocity = enemyData.velocity;
        cooldown = enemyData.cooldown;
        sprite = enemyData.sprite;
        size = enemyData.size;
        probability = enemyData.probablyItem;
        experience = enemyData.experience;
        //print("Enemigo inicializado: " + enemyName + ", " + enemyDescription);
    }
}
