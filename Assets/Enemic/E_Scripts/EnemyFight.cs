using Jugador;
using System.Collections;
using System.Collections.Generic;
using P_GameEvents;
using UnityEngine;

public class EnemyFight : MonoBehaviour
/*, IEnemyFight */ /* <--interficie, por si queremos que cada uno ataque de una manera */
{
    [SerializeField] private GameEventJint onDie;
    [SerializeField] GameObject AttackObject; // el fuego mio que ataca
    private int hp;
    private float velocidadHostia;
    private int experience;

    private Coroutine corrutinaFight;

    [SerializeField] List<Item> itemDataSO = new();
    [SerializeField] GameObject itemObject;
    private EnemyInfo enemyValues;
    private int probability;

    public void Start()
    {
        enemyValues = GetComponent<EnemyInfo>();

        hp = enemyValues.hp;
        velocidadHostia = enemyValues.cooldown;
        probability = enemyValues.probability;
        experience = enemyValues.experience;

    }

    public void StartFight()
    {
        corrutinaFight = StartCoroutine(Fight());
    }

    public void StopFight()
    {
        AttackObject.SetActive(false);
        StopCoroutine(corrutinaFight);
    }

    private IEnumerator Fight()
    {
        int i = 0;
        while (true)
        {
            i++;
            //print("exploto");
            AttackObject.SetActive(true);
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(1f);
            AttackObject.SetActive(false);
            //print("attack vuelta " + i);
            yield return new WaitForSeconds(velocidadHostia);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerAttack"))
        {
            JAttackController a = collision.GetComponent<JAttackController>();
            GetHurt(a.Player, a.Dmg);
        }
    }

    private void GetHurt(JController player, int damage)
    {
        // para que no muera dos veces, porsi....
        if (hp <= 0) return;

        hp -= damage;
        //print($"{player} me hace este da�o: {damage}");
        if (hp <= 0)
        {
            onDie.Raise(player, experience);
            LeaveItem();

            Destroy(gameObject);
        }
    }
    private void LeaveItem()
    {
        int leaveItem = Random.Range(1, probability + 1);
        if (leaveItem == probability)
        {
            GameObject obj = Instantiate(itemObject);
            obj.transform.position = this.transform.position;
            obj.GetComponent<ItemController>().InitItem(itemDataSO[Random.Range(0, itemDataSO.Count)]);
        }
    }
}