using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemy
{
    public string name { get; set; }
    public string description { get; set; }
    public Sprite sprite { get; set; }


}
