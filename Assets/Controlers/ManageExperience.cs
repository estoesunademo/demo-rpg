using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ManageExperience : MonoBehaviour
{
    public JDataSO playerData;
    public TextMeshProUGUI conceptos;
    public TextMeshProUGUI datos;
    public TextMeshProUGUI levelTitle;
    public Button button;

    public TextMeshProUGUI habilidadesOld;
    public TextMeshProUGUI habilidadesNew;
    public TextMeshProUGUI flechas;

    public bool levelUp;

    private int exp;
    private int level;
    public List<int> levelExperience = new List<int>();
    public List<int> experienceHabilityUp = new List<int>();

    public Action acabar;

    private void Awake()
    {
        levelUp = false;

        levelExperience.Add(50);   experienceHabilityUp.Add(0); // pointsToChange.Add(2);
        levelExperience.Add(100);  experienceHabilityUp.Add(1); // pointsToChange.Add(4);
        levelExperience.Add(200);  experienceHabilityUp.Add(2); // pointsToChange.Add(5);
        levelExperience.Add(600);  experienceHabilityUp.Add(4); // pointsToChange.Add(8);
        levelExperience.Add(1000); experienceHabilityUp.Add(6); // pointsToChange.Add(9);
        levelExperience.Add(2500); experienceHabilityUp.Add(9); // pointsToChange.Add(9);

        exp = playerData.experience; //Debug.Log("exp=" + exp);
        level = playerData.level; //Debug.Log("level=" + level);

        levelTitle.text = "Level " + level;
        conceptos.text = "Exp. conseguida: \r\nExp. para subir de nivel:";
        datos.text = exp + "\r\n" + levelExperience[level-1];

        habilidadesOld.text= playerData.force + "\r\n" + playerData.maxMana + "\r\n" + playerData.maxhp;
        habilidadesNew.text = "";
        flechas.GetComponent<TextMeshProUGUI>().color = Color.black;
    }
    public void Actualiza()
    {
        ActualizaGUI();

        levelTitle.text = "Level " + level;
        if (levelUp) levelTitle.GetComponent<TextMeshProUGUI>().color = Color.green;
        conceptos.text = "Exp. restante: \r\nExp. para subir de nivel:";
        datos.text = exp + "\r\n" + levelExperience[level-1];
        button.gameObject.SetActive(false);
    }
    public void ActualizaGUI()
    {
        Debug.Log("Actualiza GUI");
        int i = 1;
        while(exp > 0)
        {
            Debug.Log("Vuelta " + i);
            if ((exp - levelExperience[level - 1]) < 0)
            {
                ActualizaPlayerExp();

                habilidadesOld.GetComponent<TextMeshProUGUI>().color = Color.grey;
                flechas.color = Color.red;
                if(levelUp) habilidadesNew.GetComponent<TextMeshProUGUI>().color = Color.green;
                habilidadesNew.text = playerData.force + "\r\n" + playerData.maxMana + "\r\n" + playerData.maxhp;
                

                return;
            }
            exp-= levelExperience[level - 1];
            if (exp >= 0)
            {
                level++;
                levelUp = true;
                ActualizaJSO();
            }
         
        }
        
    }

    private void ActualizaJSO()
    {
        playerData.force += experienceHabilityUp[level - 1];
        playerData.maxhp += experienceHabilityUp[level - 1] * 3;
        playerData.maxMana += experienceHabilityUp[level - 1] * 3;
    }

    public void ActualizaPlayerExp()
    {
        Debug.Log("Actualiza SO-PLAYER");
        playerData.experience = exp;
        playerData.level = level;
    }
    public void HeAcabado()
    {
        acabar.Invoke();
    }
}
