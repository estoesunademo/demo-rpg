using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "LevelSO")]
public class Level : ScriptableObject
{
   public int levelAt;
}
