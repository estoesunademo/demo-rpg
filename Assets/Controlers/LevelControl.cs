using Jugador;
using P_GameEvents;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelControl : MonoBehaviour
{
    public Level level;
    [SerializeField] private int levelActual = 1;
    [SerializeField] private List<int> levelsAndEnemies = new();
    [SerializeField] private GameEventInt onLevelEnd;

    private void Awake()
    {
        levelActual = level.levelAt;
        Debug.Log("LevelControler: estamos en el nivel: "+ levelActual);

        levelsAndEnemies.Add(3);
        levelsAndEnemies.Add(6);
        levelsAndEnemies.Add(9);
    }
    public void EnemyCount(JController x, int y)
    {
        //Debug.Log("Debug1--->" +levelsAndEnemies[0]+", x -->" + x);
        levelsAndEnemies[levelActual - 1] -= 1;
        //Debug.Log("Enemigo eliminado, quedan " + levelsAndEnemies[levelActual - 1]);
        if(levelsAndEnemies[levelActual - 1] == 0)
        {
            Debug.Log("nivel finalizado");
            level.levelAt+=1;
            Debug.Log("HAS PASADO AL NIVEL: " + level.levelAt);
            //lanzar evento NIVEL UP!
            onLevelEnd.Raise(level.levelAt);
        }
    }


    
}
