using System;
using Jugador;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceManager : MonoBehaviour
{
    public int num;
    public List<ManageExperience> mManageExperienceList;
    public GameManagerController gameManagerController;

    private void Awake()
    {
        num = 0;
    }

    private void Start()
    {
        if (mManageExperienceList.Count < 1) return;
        mManageExperienceList[0].acabar += onAcabarActualizacionExp;
        mManageExperienceList[1].acabar += onAcabarActualizacionExp;
    }
    public void AcumularExp(JController jc, int experience)
    { 
        jc.GetComponent<JDataController>().GetExperience(experience);
    }

    public void onAcabarActualizacionExp()
    {
        num++;
        Debug.Log("HA acabado " + num);
        if(num == 2)
        {
            StartCoroutine(acabar());
        }
    }

    IEnumerator acabar()
    {
        yield return new WaitForSeconds(2);
        gameManagerController.NextLevel();
    }

}
