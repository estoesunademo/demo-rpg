using Jugador;
using Jugador.Inventory;
using Mapa.camera;
using P_GameEvents;
using UnityEngine;

namespace Mapa
{
    public class JSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject canvas;

        [Header("J1")]
        //pesao

        [SerializeField] private JDataSO soJ1;

        [SerializeField] private SOInventory soIJ1;
        [SerializeField] private GameEventInt onJ1IndexChange;
        [SerializeField] private GameObject InventoryGUIJ1;


        [Header("J2")]
        // pesao

        [SerializeField] private JDataSO soJ2;

        [SerializeField] private SOInventory soIJ2;
        [SerializeField] private GameEventInt onJ2IndexChange;
        [SerializeField] private GameObject InventoryGUIJ2;


        private void Start()
        {
            GameObject canvass = Instantiate(canvas);
            canvass.GetComponent<RectTransform>().position = new Vector3(0, 0, 0);

            var j1 = Instantiate(GameManagerController.Instance.J1);
            GameObject iGuiJ1 = Instantiate(InventoryGUIJ1, canvass.transform);
            initPlayer(j1, soJ1, soIJ1, onJ1IndexChange, iGuiJ1, -960, "J1", GameManagerController.Instance.j1type);


            var j2 = Instantiate(GameManagerController.Instance.J2);
            GameObject iGuiJ2 = Instantiate(InventoryGUIJ2, canvass.transform);
            initPlayer(j2, soJ2, soIJ2, onJ2IndexChange, iGuiJ2, 960, "J2", GameManagerController.Instance.j2type);
        }

        private void initPlayer(GameObject player, JDataSO stats, SOInventory inventory, GameEventInt onIndexChange,
            GameObject iGui, int position, string scheme, JPlayerType playerType)
        {
            player.GetComponent<JController>().playerType = playerType;
            player.GetComponent<JController>().ControlScheme = scheme;
            //stats.hp = 100;
            //stats.mana = 100;
            player.GetComponent<JDataController>().MPlayerData = stats;
            player.GetComponent<JInventory>().MInventory = inventory;
            player.GetComponent<JInventory>().OnIndexChange = onIndexChange;
            player.transform.position = new Vector3(-1, 0, -1);
            CameraController.Instance.AddJugador(player);
            iGui.GetComponent<RectTransform>().position = new Vector3(position, 0, 0);
        }
    }
}