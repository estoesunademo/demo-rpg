using System.Collections.Generic;
using Jugador;
using Mapa.camera;
using UnityEngine;
using UnityEngine.Serialization;

namespace Mapa
{
    public class LvlController : MonoBehaviour
    {
        [SerializeField] private bool isOn = false;
        [SerializeField] private new string name = "lvl1";

        private readonly List<JController> _players = new();
        private void OnTriggerEnter2D(Collider2D other)
        {
            print("enter " + name);
            if (!other.CompareTag("Player")) return;
            _players.Add(other.GetComponent<JController>());
            if (_players.Count < CameraController.Instance.Jugadors.Count) return;
            print("enter map");
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (!other.CompareTag("Player")) return;
            _players.Remove(other.GetComponent<JController>());
        }
    }
}
