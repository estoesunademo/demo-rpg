using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Mapa.camera
{
    public class CameraController : MonoBehaviour
    {
        public static CameraController Instance { get; private set; }

        [SerializeField] private List<GameObject> jugadors;

        public List<GameObject> Jugadors
        {
            get => jugadors;
            set => jugadors = value;
        }


        private Transform _target;

        [SerializeField] private Bounds mScreenBounds;

        public Bounds CameraBounds => mScreenBounds;
    
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            _target = gameObject.GetComponent<Transform>();
        }

        public void AddJugador(GameObject j)
        {
            Jugadors.Add(j);
        }

        private void LateUpdate()
        {
            if (Jugadors.Count < 1) return;
            var middlePosition = Jugadors.Aggregate(Vector3.zero, (current, obj) => current + obj.transform.position);

            if (Jugadors.Count > 0)
            {
                middlePosition /= Jugadors.Count;
            }

            _target.position = new Vector3(middlePosition.x, middlePosition.y, -10);
            UpdateBounds();
        }

        private void UpdateBounds()
        {
            mScreenBounds.center = transform.position;
            var minBounds = GetComponent<Camera>().ViewportToWorldPoint(Vector2.zero);
            minBounds.z = -10;
            var maxBounds = GetComponent<Camera>().ViewportToWorldPoint(Vector2.one);
            maxBounds.z = 10;
            mScreenBounds.min = minBounds;
            mScreenBounds.max = maxBounds;
        }
    }
}