using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class GUIStatsController : MonoBehaviour
{
    /*[SerializeField] private JDataSO StatsSO;*/
    [SerializeField] private TextMeshProUGUI hp;
    [SerializeField] private TextMeshProUGUI mana;
    [SerializeField] private JDataSO playerStats;

    public JDataSO PlayerStats
    {
        get => playerStats;
        set => playerStats = value;
    }

    private void Awake()
    {
        FillDisplay();
    }

    public void FillDisplay()
    {
        hp.text = "Hp: " + playerStats.hp + " / " + playerStats.maxhp;
        mana.text = "Mana: " + playerStats.mana + " / " + playerStats.maxMana;
    }
}