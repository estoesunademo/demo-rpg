using System;
using System.Collections.Generic;
using System.IO;
using Jugador;
using SaveLoad;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerController : MonoBehaviour
{
    public static GameManagerController Instance { get; private set; }

    public Level levelAt;

    // controla el nivel en el que estamos
    public LevelControl levelControl;

    [SerializeField] private GameObject mag;
    [SerializeField] private GameObject archer;
    [SerializeField] private GameObject warrior;

    [SerializeField] private JDataSO soJ1;
    [SerializeField] private JDataSO soJ2;

    [SerializeField] private GameObject j1;
    [SerializeField] private GameObject j2;

    [SerializeField] private string firstScene;

    public JPlayerType j1type;
    public JPlayerType j2type;

    public GameObject J1 => j1;
    public GameObject J2 => j2;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(this);

        // j1 = mag;
        // j2 = mag;

        // iniciamos siempre en el nivel 1
        levelAt.levelAt = 1;
    }

    private void Start()
    {
        SetPlayer1(0);
        SetPlayer2(0);
    }

    public void SetPlayer1(int n)
    {
        j1 = GetClass(n);
        j1type = GetClassType(n);
        print(j1type);
    }

    public void SetPlayer2(int n)
    {
        j2 = GetClass(n);
        j2type = GetClassType(n);
        print(j2type);
    }

    private GameObject GetClass(int n)
    {
        return n switch
        {
            0 => mag,
            1 => archer,
            2 => warrior,
            _ => null
        };
    }

    private JPlayerType GetClassType(int n)
    {
        return n switch
        {
            0 => JPlayerType.Wizard,
            1 => JPlayerType.Archer,
            2 => JPlayerType.Warrior,
            _ => JPlayerType.None
        };
    }

    public void ChangeScene(string s)
    {
        SceneManager.LoadScene(s);
    }

    public void StartGame()
    {
        soJ1.hp = 100;
        soJ1.mana = 100;
        soJ2.hp = 100;
        soJ2.mana = 100;
        SceneManager.LoadScene(firstScene);
    }

    public void LoadGame()
    {
        try
        {
            string jsonData = File.ReadAllText(MSaveDataManager.saveFileName);
            SaveData data = new SaveData();
            JsonUtility.FromJsonOverwrite(jsonData, data);
            j1 = GetClass((int)data.m_Players[0].playerType);
            j2 = GetClass((int)data.m_Players[1].playerType);

            j1type = data.m_Players[0].playerType;
            j2type = data.m_Players[1].playerType;

            soJ1.hp = data.m_Players[0].hp;
            soJ1.mana = data.m_Players[0].mana;
            soJ1.level = data.m_Players[0].level;

            soJ2.hp = data.m_Players[1].hp;
            soJ2.mana = data.m_Players[1].mana;
            soJ2.level = data.m_Players[1].level;

            SceneManager.LoadScene(firstScene);
        }
        catch (Exception e)
        {
            Debug.LogError(
                $"Error while trying to load {Path.Combine(Application.persistentDataPath, MSaveDataManager.saveFileName)} with exception {e}");
        }
    }

    public void NextLevel()
    {
        SceneManager.LoadScene("Level" + levelAt.levelAt.ToString());
    }


    public void EndLevelToExperienceScene(int i)
    {
        Debug.Log("Iniciamos nuevo NIVEL: " + i);
        String newLevel = "Level" + i.ToString();
        Debug.Log(newLevel);
        ChangeScene("ExperienceScene"); 
    }
}