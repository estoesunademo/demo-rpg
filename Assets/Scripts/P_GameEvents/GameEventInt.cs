using UnityEngine;

namespace P_GameEvents
{
    [CreateAssetMenu(fileName = "GameEventGenericInt", menuName = "GameEvent/GameEventGeneric/GameEvent(int)")]
    public class GameEventInt : GameEventGeneric<int>
    {
    }
}