using System.Collections;
using System.Collections.Generic;
using Jugador;
using UnityEngine;

namespace P_GameEvents
{
    [CreateAssetMenu(fileName = "GameEventGenericJint",
        menuName = "GameEvent/GameEventGeneric/GameEvent(JController, int)")]
    public class GameEventJint : GameEvent<JController, int>
    {
    }
}