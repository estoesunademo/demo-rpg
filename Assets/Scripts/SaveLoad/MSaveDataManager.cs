using System;
using System.IO;
using Jugador;
using UnityEngine;

namespace SaveLoad
{
    public class MSaveDataManager : MonoBehaviour
    {
        public const string saveFileName = "sassabasavadodajova.json";

        public void SaveData()
        {
            print("save data");
            SaveData.ISaveableObject[] saveableObjects = FindObjectsByType<JController>(FindObjectsSortMode.None);
            SaveData data = new SaveData();
            data.PopulateData(saveableObjects);
            string jsonData = JsonUtility.ToJson(data);

            try
            {
                Debug.Log("Saving: ");
                Debug.Log(jsonData);

                File.WriteAllText(saveFileName, jsonData);
            }
            catch (Exception e)
            {
                Debug.LogError(
                    $"Error while trying to save {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
            }
        }
    }
}