using System;
using Jugador;

namespace SaveLoad
{
    [Serializable]
    public class SaveData
    {
        [Serializable]
        public struct PlayerData
        {
            /* experience*/
            public int level;
            public int experience;

            public int speed;
            public int force;
            public int mana;
            public int hp;
            public int maxHp;
            public int maxMana;
            public JPlayerType playerType;

            public PlayerData(int level, int experience, int speed, int force, int mana, int hp, int maxHp, int maxMana
                , JPlayerType playerType)
            {
                this.level = level;
                this.experience = experience;
                this.speed = speed;
                this.force = force;
                this.mana = mana;
                this.hp = hp;
                this.maxHp = maxHp;
                this.maxMana = maxMana;
                this.playerType = playerType;
            }
        }

        public PlayerData[] m_Players;

        public void PopulateData(ISaveableObject[] _playerData)
        {
            m_Players = new PlayerData[_playerData.Length];
            for (int i = 0; i < _playerData.Length; i++)
                m_Players[i] = _playerData[i].Save();
        }


        public interface ISaveableObject
        {
            public PlayerData Save();
            public void Load(PlayerData playerData);
        }
    }
}