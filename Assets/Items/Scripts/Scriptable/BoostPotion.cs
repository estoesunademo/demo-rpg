using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Potion", menuName = "Inventory/Items/Potion")]
public class BoostPotion : Item
{
    [Header("Potion Type")] [SerializeField]
    private PotionType m_PotionType;

    [Header("Healing")] [SerializeField] [Min(0f)]
    private int m_Healing;

    public override bool UsedBy(GameObject go)
    {
        if (!go.TryGetComponent(out IHealable healable)) return false;
        switch (m_PotionType)
        {
            case PotionType.Health:
                healable.Heal(m_Healing);
                break;
            case PotionType.Mana:
                healable.Mana(m_Healing);
                break;
        }
        return true;
    }
}