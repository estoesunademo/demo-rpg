using Items.Scripts.Type;
using UnityEngine;

[CreateAssetMenu(fileName = "Gun", menuName = "Inventory/Items/Gun")]
public class Gun : Item
{
    [Header("Gun Type")] [SerializeField] private GunType m_GunType;

    [Header("Damage")] [SerializeField] [Min(0f)]
    private int damage;

    public int Damage
    {
        get => damage;
        set => damage = value;
    }
    public override bool UsedBy(GameObject go)
    {
        throw new System.NotImplementedException();
    }
}