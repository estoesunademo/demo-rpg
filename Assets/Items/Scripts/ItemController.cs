using System;
using UnityEngine;

public class ItemController : MonoBehaviour
{
    [SerializeField] private Item _item;

    private void Start()
    {
        InitItem(_item);
    }

    public void InitItem(Item item)
    {
        this._item = item;
        GetComponent<SpriteRenderer>().sprite = item.Sprite;
    }

    public Item Item => _item;
}