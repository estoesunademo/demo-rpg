using System;
using System.Collections;
using System.Collections.Generic;
using TreeEditor;
using UnityEngine;

public class ChestController : MonoBehaviour
{
    [SerializeField] private Gun _ItemGun;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }


    public void Empty()
    {
        _spriteRenderer.color = Color.red;
        ItemGun = null;
    }

    public SpriteRenderer SpriteRenderer
    {
        get => _spriteRenderer;
        set => _spriteRenderer = value;
    }

    public Gun ItemGun
    {
        get => _ItemGun;
        set => _ItemGun = value;
    }
}