using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface I_Item
{
    public string Id { get; set; }
    public string Description { get; set; }
    public Sprite Sprite { get; set; }
    public bool UsedBy(GameObject go);
}