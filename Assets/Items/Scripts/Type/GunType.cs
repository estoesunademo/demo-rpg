﻿namespace Items.Scripts.Type
{
    public enum GunType
    {
        Sword,
        Bow,
        Spell
    }
}