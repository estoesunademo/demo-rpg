using System;
using Mapa.camera;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Jugador
{
    public class JMovement : MonoBehaviour
    {
        private InputAction _movement;
        public event Action OnBoundriesExit;
    
        [SerializeField] private int speed = 3;
        private void Start()
        {
            _movement = GetComponent<JController>().getMovement();
        }
    
        public void Move(float multiplier)
        {
            Vector3 movement = _movement.ReadValue<Vector2>() * (speed * multiplier);
            GetComponent<Rigidbody2D>().AddForce(movement);
            if (GetComponent<Rigidbody2D>().velocity.magnitude > 3) GetComponent<Rigidbody2D>().velocity = movement;
            transform.up = movement;
            if(IsOutOfBounds(movement)) OnBoundriesExit?.Invoke();
        }

        private bool IsOutOfBounds(Vector3 movement)
        {
            var newPosition = movement * Time.fixedDeltaTime + transform.position;
            return !CameraController.Instance.CameraBounds.Contains(newPosition);
        }
    
    }
}
