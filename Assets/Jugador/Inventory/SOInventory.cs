﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;

namespace Jugador.Inventory
{
    [CreateAssetMenu(fileName = "Inventory", menuName = "Inventory/Inventory")]
    public class SOInventory : ScriptableObject
    {
        [Header("Index")] [SerializeField] public int invIndex = 0;
        
        [Header("Game Events")] [SerializeField]
        private GameEvent onInventoryUpdate;

        [Header("Gun")] [SerializeField] private Gun gun;
        public Gun Gun => gun;

        [Serializable]
        public class ItemSlot
        {
            [SerializeField] public Item Item;
            [SerializeField] public int Amount;

            public ItemSlot(Item obj)
            {
                Item = obj;
                Amount = 1;
            }
        }

        [SerializeField] private List<ItemSlot> m_ItemSlots = new List<ItemSlot>(3);

        public ReadOnlyCollection<ItemSlot> ItemSlots => new ReadOnlyCollection<ItemSlot>(m_ItemSlots);

        public void AddItem(Item usedItem)
        {
            ItemSlot item = GetItem(usedItem);
            if (item == null)
                m_ItemSlots.Add(new ItemSlot(usedItem));
            else
                item.Amount++;
            onInventoryUpdate.Raise();
        }

        public void AddGun(Gun pGun)
        {
            gun = pGun;
        }

        public void RemoveItem(Item usedItem)
        {
            ItemSlot item = GetItem(usedItem);
            if (item == null)
                return;
            item.Amount--;
            if (item.Amount <= 0)
                m_ItemSlots.Remove(item);
        }

        private ItemSlot GetItem(Item item)
        {
            return m_ItemSlots.FirstOrDefault(slot => slot.Item == item);
        }
    }
}