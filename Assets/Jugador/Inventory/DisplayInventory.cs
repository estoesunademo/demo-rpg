using System;
using System.Collections;
using System.Collections.Generic;
using Jugador.Inventory;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DisplayInventory : MonoBehaviour
{
    [SerializeField] private GameObject m_DisplayItemPrefab;

    [SerializeField] private SOInventory m_Inventory;

    private GameObject selectedItem;

    public SOInventory MInventory
    {
        get => m_Inventory;
        set => m_Inventory = value;
    }

    private void Start()
    {
        RefreshBackpack();
    }

    private void ClearDisplay()
    {
        foreach (Transform child in transform)
            Destroy(child.gameObject);
    }

    private void FillDisplay()
    {
        foreach (SOInventory.ItemSlot itemSlot in m_Inventory.ItemSlots)
        {
            GameObject displayedItem = Instantiate(m_DisplayItemPrefab, transform);
            displayedItem.GetComponent<DisplayItem>().Load(itemSlot);
        }
    }

    public void SelectItem(int index)
    {
        if (selectedItem != null)
            selectedItem.GetComponent<Image>().color = Color.white;
        selectedItem = transform.GetChild(m_Inventory.invIndex).gameObject;
        selectedItem.GetComponent<Image>().color = Color.red;
    }

    public void RefreshBackpack()
    {
        RefreshAux();
        SelectItem(0);
    }

    public void RefreshAux()
    {
        ClearDisplay();
        FillDisplay();
    }
}