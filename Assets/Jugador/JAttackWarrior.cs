using System;
using System.Collections;
using UnityEngine;

namespace Jugador
{
    public class AttackWarrior : MonoBehaviour, IJAttack
    {
        [SerializeField] private GameObject sword;
        [SerializeField] private GameEvent OnJStatsChange;
        public event Action OnAttackEnd;
        private JController _jc;
        
        private void Awake()
        {
            sword.SetActive(false);
            _jc = GetComponent<JController>();
        }

        public void Attack()
        {
            if (_jc.GetComponent<JDataController>().MPlayerData.mana < 1)
            {
                OnAttackEnd?.Invoke();
                return;
            }
            _jc.GetComponent<JDataController>().MPlayerData.mana -= 1;
            OnAttackEnd?.Invoke();
            int gunDamage = 0;
            if (GetComponent<JInventory>().MInventory.Gun)
            {
                gunDamage = GetComponent<JInventory>().MInventory.Gun.Damage;
            }
            sword.GetComponent<JAttackController>().Dmg = _jc.GetComponent<JDataController>().MPlayerData.force + gunDamage;
            sword.GetComponent<JAttackController>().Player = GetComponent<JController>();
            sword.SetActive(true);
            OnJStatsChange.Raise();
            StartCoroutine(EndAttack());
        }

        private IEnumerator EndAttack()
        {
            yield return new WaitForSeconds(1);
            sword.SetActive(false);
        }
    }
}
