using Mapa.camera;
using UnityEngine;

namespace Jugador
{
    public class JHurtbox : MonoBehaviour
    {
        [SerializeField] private GameEvent onJStatsChange;
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("EnemyAttack")) return;
            GetComponent<JController>().GetComponent<JDataController>().MPlayerData.hp -= other.GetComponent<EnemyHitBox>().damage;
            onJStatsChange.Raise();
            if (GetComponent<JController>().GetComponent<JDataController>().MPlayerData.hp > 0) return;
            CameraController.Instance.Jugadors.Remove(gameObject);
            Destroy(gameObject);
        }
    }
}
