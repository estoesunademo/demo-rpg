using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Data", menuName = "Player S.O.")]
public class JDataSO : ScriptableObject
{
    [Header("GESTOR de EXPERIENCIA")] public int level;
    public int experience;

    [Header("CARACTERISTICAS del Jugador")]
    public int speed;
    public int force;
    public int mana;
    public int hp;
    public int maxhp;
    public int maxMana;
}