using System;

namespace Jugador
{
    public interface IJAttack
    {
        public void Attack();
        
        public event Action OnAttackEnd;
    }
}
