using System;
using System.Collections;
using UnityEngine;

namespace Jugador
{
    public class AttackArcher : MonoBehaviour, IJAttack
    {
        [SerializeField] private GameObject _arrow;
        [SerializeField] private GameEvent OnJStatsChange;
        private JController _jc;
        private int _dmg;

        private void Start()
        {
            _jc = GetComponent<JController>();
            _dmg = GetComponent<JDataController>().MPlayerData.force;
        }

        public event Action OnAttackEnd;

        public void Attack()
        {
            if (_jc.GetComponent<JDataController>().MPlayerData.mana < 5)
            {
                OnAttackEnd?.Invoke();
                return;
            }

            _jc.GetComponent<JDataController>().MPlayerData.mana -= 5;
            GameObject newArrow = Instantiate(_arrow);
            var transform1 = transform;
            newArrow.transform.position = transform1.position;
            Vector3 up = transform1.up;
            newArrow.transform.up = up;
            newArrow.GetComponent<Rigidbody2D>().velocity = up * 10;
            int gunDamage = 0;
            if (GetComponent<JInventory>().MInventory.Gun)
            {
                gunDamage = GetComponent<JInventory>().MInventory.Gun.Damage;
            }

            newArrow.GetComponent<JAttackController>().Dmg = _dmg + gunDamage;
            /* */
            newArrow.GetComponent<JAttackController>().Player = GetComponent<JController>();
            OnAttackEnd?.Invoke();
            OnJStatsChange.Raise();
        }
    }
}