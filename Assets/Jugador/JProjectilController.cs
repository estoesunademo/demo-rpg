using System;
using System.Collections;
using UnityEngine;

namespace Jugador
{
    public class JProjectilController : MonoBehaviour
    {
        private void Start()
        {
            PlayAudio();
            StartCoroutine(Kms());
        }

        private IEnumerator Kms()
        {
            yield return new WaitForSeconds(1f);
            Destroy(gameObject);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Player") ||
                other.GetComponent<JController>() == GetComponent<JAttackController>().Player)
                return;
            print("soy proyectil y he chocado con un player");
            Rigidbody2D rigido = GetComponent<Rigidbody2D>();
            rigido.velocity = -rigido.velocity;
        }
        
        private void PlayAudio() {
            GetComponent<AudioSource>().Play();    
        }
    }
}