﻿using System;
using Jugador.Inventory;
using P_GameEvents;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Jugador
{
    public class JInventory : MonoBehaviour
    {
        [SerializeField] private SOInventory m_Inventory;
        [SerializeField] private GameEventInt onIndexChange;
        [SerializeField] private GameEvent onInventoryUpdate;

        public SOInventory MInventory
        {
            get => m_Inventory;
            set => m_Inventory = value;
        }

        public GameEventInt OnIndexChange
        {
            set => onIndexChange = value;
        }

        private void Start()
        {
            //onIndexChange.Raise(m_Inventory.invIndex);
            Debug.Log("Current inventory index: " + m_Inventory.invIndex);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!(other.CompareTag("Item") || other.CompareTag("Chest"))) return;
            PickUpItem(other);
        }

        private void PickUpItem(Collider2D other)
        {
            if (other.GetComponent<ChestController>() == null)
            {
                AddItem(other.GetComponent<ItemController>().Item);
                Destroy(other.gameObject);
                print(("he afagat una poció!!!!!!!!!!!!!!!!"));
            }
            else
            {
                ChestController chestController = other.GetComponent<ChestController>();
                if (chestController.ItemGun == null || m_Inventory.Gun != null) return;
                m_Inventory.AddGun(chestController.ItemGun);
                chestController.Empty();
                print(("he afagat un arma!!!!!!!!!!!!!!!!"));
            }
        }

        public void AddItem(Item item)
        {
            m_Inventory.AddItem(item);
        }

        public void IterateItem(InputAction.CallbackContext context)
        {
            m_Inventory.invIndex = (m_Inventory.invIndex + 1) % m_Inventory.ItemSlots.Count;
            Debug.Log("Current inventory index: " + m_Inventory.invIndex);
            onIndexChange.Raise(0);
        }

        public void UseItem(InputAction.CallbackContext context)
        {
            if (m_Inventory.ItemSlots[m_Inventory.invIndex].Amount < 1)
                return;
            if (!m_Inventory.ItemSlots[m_Inventory.invIndex].Item.UsedBy(gameObject))
                return;
            m_Inventory.RemoveItem(m_Inventory.ItemSlots[m_Inventory.invIndex].Item);
            onInventoryUpdate.Raise();
        }
    }
}