using UnityEngine;

namespace Jugador
{
    public class JAttackController : MonoBehaviour
    {
        private JController _player;

        [SerializeField] private int _dmg;

        public int Dmg
        {
            get => _dmg;
            set => _dmg = value;
        }

        public JController Player
        {
            get => _player;
            set => _player = value;
        }
    }
}