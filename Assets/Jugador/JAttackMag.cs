using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Jugador
{
    public class AttackMag : MonoBehaviour, IJAttack
    {
        [SerializeField] private GameObject fireball;
        [SerializeField] private GameEvent OnJStatsChange;
        public event Action OnAttackEnd;
        private JController _jc;

        private void Start()
        {
            _jc = GetComponent<JController>();
        }
        public void Attack()
        {
            if (_jc.GetComponent<JDataController>().MPlayerData.mana < 10)
            {
                OnAttackEnd?.Invoke();
                return;
            }
            _jc.GetComponent<JDataController>().MPlayerData.mana -= 10;
            GameObject newFireball = Instantiate(fireball);
            var transform1 = transform;
            newFireball.transform.position = transform1.position;
            Vector3 up = transform1.up;
            newFireball.transform.up = up;
            newFireball.GetComponent<Rigidbody2D>().velocity = up * 10;
            int gunDamage = 0;
            if (GetComponent<JInventory>().MInventory.Gun)
            {
                gunDamage = GetComponent<JInventory>().MInventory.Gun.Damage;
            }
            newFireball.GetComponent<JAttackController>().Dmg = _jc.GetComponent<JDataController>().MPlayerData.force + gunDamage;
            newFireball.GetComponent<JAttackController>().Player = GetComponent<JController>();
            OnAttackEnd?.Invoke();
            OnJStatsChange.Raise();
        }
    }
}
