using System;
using System.Collections;
using Jugador.Inventory;
using SaveLoad;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Jugador
{
    [RequireComponent(typeof(JMovement))]
    [RequireComponent(typeof(JHurtbox))]
    [RequireComponent(typeof(JInventory))]
    [RequireComponent(typeof(JDataController))]
    public class JController : MonoBehaviour, SaveData.ISaveableObject
    {
        private enum SwitchMachineStates
        {
            Idle,
            Walk,
            Run,
            Attack,
            Jump,
            Dead
        }

        private SwitchMachineStates _mCurrentState;

        [SerializeField] private InputActionAsset pInputAsset;
        private InputActionAsset _pInput;

        private string _controlScheme;

        public string ControlScheme
        {
            get => _controlScheme;
            set
            {
                _controlScheme = value;
                SetInputBindings();
            }
        }

        //---PROPIETATS---//
        public JPlayerType playerType;
        private Rigidbody2D _pRigidbody;
        private InputAction _pMovement;
        private Transform _pTransform;
        private Animator _pAnimator;


        //---COMPONENTS---//
        private JMovement _cMovement;
        private IJAttack _cAttack;
        private JHurtbox _cHurtbox;
        private JInventory _cInventory;
        private JDataController c_DataController;

        private void Awake()
        {
            c_DataController = GetComponent<JDataController>();
            _cInventory = GetComponent<JInventory>();
            _cMovement = GetComponent<JMovement>();
            _cMovement.OnBoundriesExit += OutOfBounds;
            _cAttack = GetComponent<IJAttack>();
            _cAttack.OnAttackEnd += EndAttack;

            _pRigidbody = GetComponent<Rigidbody2D>();
            _pTransform = GetComponent<Transform>();
            _pInput = Instantiate(pInputAsset);
        }

        private void SetInputBindings()
        {
            _pInput.bindingMask = InputBinding.MaskByGroup(ControlScheme);
            InputActionMap gameplay = _pInput.FindActionMap("Gameplay");
            _pMovement = gameplay.FindAction("Move");
            gameplay.FindAction("Run").started += StartRun;
            gameplay.FindAction("Run").canceled += EndRun;
            gameplay.FindAction("Attack").started += StartAttack;
            gameplay.FindAction("Iterate").started += _cInventory.IterateItem;
            gameplay.FindAction("Use").started += _cInventory.UseItem;
            gameplay.Enable();
        }

        private void Start()
        {
            InitState(SwitchMachineStates.Idle);
            //StartCoroutine(RegenerateMana());
        }

        //CANVIAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //CANVIAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //CANVIAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //CANVIAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //CANVIAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //CANVIAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // private IEnumerator RegenerateMana()
        // {
        //     while (true)
        //     {
        //         yield return new WaitForSeconds(1);
        //         if (data.mana < 100) data.mana += regen;
        //     }
        // }

        private void Update()
        {
            UpdateState();
        }

        public void Unsubscribe(int i)
        {
            InputActionMap gameplay = _pInput.FindActionMap("Gameplay");
            gameplay.FindAction("Run").started -= StartRun;
            gameplay.FindAction("Run").canceled -= EndRun;
            gameplay.FindAction("Attack").started -= StartAttack;
            gameplay.FindAction("Iterate").started -= _cInventory.IterateItem;
            gameplay.FindAction("Use").started -= _cInventory.UseItem;
        }

        private void StartRun(InputAction.CallbackContext c)
        {
            ChangeState(SwitchMachineStates.Run);
        }

        private void EndRun(InputAction.CallbackContext c)
        {
            ChangeState(SwitchMachineStates.Walk);
        }

        private void StartAttack(InputAction.CallbackContext c)
        {
            ChangeState(SwitchMachineStates.Attack);
        }

        private void EndAttack()
        {
            ChangeState(SwitchMachineStates.Idle);
        }

        private void OutOfBounds()
        {
            ChangeState(SwitchMachineStates.Idle);
        }

        public InputAction getMovement()
        {
            return _pMovement;
        }

        private void ChangeState(SwitchMachineStates newState)
        {
            ExitState();
            InitState(newState);
        }

        private void InitState(SwitchMachineStates currentState)
        {
            _mCurrentState = currentState;
            switch (_mCurrentState)
            {
                case SwitchMachineStates.Idle:
                    _pRigidbody.velocity = Vector2.zero;
                    break;
                case SwitchMachineStates.Walk:
                    break;
                case SwitchMachineStates.Run:
                    break;
                case SwitchMachineStates.Attack:
                    _cAttack.Attack();
                    break;
                case SwitchMachineStates.Jump:
                    break;
                case SwitchMachineStates.Dead:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void UpdateState()
        {
            switch (_mCurrentState)
            {
                case SwitchMachineStates.Idle:
                    if (_pMovement.ReadValue<Vector2>() != Vector2.zero)
                        ChangeState(SwitchMachineStates.Walk);
                    break;
                case SwitchMachineStates.Walk:
                    if (_pMovement.ReadValue<Vector2>() != Vector2.zero)
                        _cMovement.Move(1);
                    else ChangeState(SwitchMachineStates.Idle);
                    break;
                case SwitchMachineStates.Run:
                    _cMovement.Move(2);
                    break;
                case SwitchMachineStates.Attack:
                    break;
                case SwitchMachineStates.Jump:
                    break;
                case SwitchMachineStates.Dead:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ExitState()
        {
            switch (_mCurrentState)
            {
                case SwitchMachineStates.Idle:
                    break;
                case SwitchMachineStates.Walk:
                    break;
                case SwitchMachineStates.Attack:
                    break;
                case SwitchMachineStates.Jump:
                    break;
                case SwitchMachineStates.Dead:
                    break;
                case SwitchMachineStates.Run:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public SaveData.PlayerData Save()
        {
            return new SaveData.PlayerData(c_DataController.MPlayerData.level, c_DataController.MPlayerData.experience,
                c_DataController.MPlayerData.speed, c_DataController.MPlayerData.force,
                c_DataController.MPlayerData.mana, c_DataController.MPlayerData.hp, c_DataController.MPlayerData.maxhp,
                c_DataController.MPlayerData.maxMana, playerType);
        }

        public void Load(SaveData.PlayerData playerData)
        {
            c_DataController.MPlayerData.level = playerData.level;
            c_DataController.MPlayerData.experience = playerData.experience;
            c_DataController.MPlayerData.speed = playerData.speed;
            c_DataController.MPlayerData.force = playerData.force;
            c_DataController.MPlayerData.mana = playerData.mana;
            c_DataController.MPlayerData.hp = playerData.hp;
            c_DataController.MPlayerData.maxhp = playerData.maxHp;
            c_DataController.MPlayerData.maxMana = playerData.maxMana;
            playerType = playerData.playerType;
        }
    }
}