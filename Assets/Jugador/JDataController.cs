using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JDataController : MonoBehaviour, IHealable
{
    [SerializeField] private JDataSO m_playerData;
    [SerializeField] private GameEvent onJStatsChange;
    [SerializeField] private int regen;
    
    public JDataSO MPlayerData
    {
        get => m_playerData;
        set => m_playerData = value;
    }

    private void Start()
    {
        StartCoroutine(RegenerateMana());
    }

    private IEnumerator RegenerateMana()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            if (MPlayerData.mana + regen <= MPlayerData.maxMana) MPlayerData.mana += regen;
            onJStatsChange.Raise();
        }
    }

    public void Heal(int healAmount)
    {
        int newHp = m_playerData.hp + healAmount;
        m_playerData.hp = newHp > m_playerData.maxhp ? 100 : newHp;
        onJStatsChange.Raise();
    }

    public void Mana(int manaAmount)
    {
        int newMana = m_playerData.mana + manaAmount;
        m_playerData.mana = newMana > m_playerData.maxMana ? 100 : newMana;
        onJStatsChange.Raise();
    }

    /* manu */
    public void GetExperience(int exp)
    {
        m_playerData.experience += exp;
    }
}